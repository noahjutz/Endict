package com.noahjutz.endict.ui.definitions.search

import androidx.compose.animation.Crossfade
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.noahjutz.endict.api.DatabaseDefinition
import com.noahjutz.endict.api.GcideDatabase
import com.noahjutz.endict.ui.definitions.definition_content.ErrorAlert
import com.noahjutz.endict.ui.definitions.definition_content.WordDetail
import com.noahjutz.endict.ui.definitions.definition_content.WordDetailShimmer
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext

private sealed class State {
    object Loading : State()
    data class Error(val error: Exception) : State()
    data class Found(val definitions: List<DatabaseDefinition>) : State()
}

@ExperimentalFoundationApi
@ExperimentalMaterialApi
@Composable
fun SearchBottomSheet(searchQuery: String) {
    Card(shape = RoundedCornerShape(topStart = 24.dp, topEnd = 24.dp)) {
        Box(Modifier.fillMaxSize()) {
            var state: State by remember { mutableStateOf(State.Loading) }
            val context = LocalContext.current
            LaunchedEffect(searchQuery) {
                state = State.Loading
                val definitions = withContext(IO) {
                    GcideDatabase.getDatabase(context).definitionDao().getDefinitions(searchQuery)
                }
                state =
                    if (definitions.isEmpty()) State.Error(Exception("Not found"))
                    else State.Found(definitions)
            }

            Crossfade(state) {
                when (it) {
                    is State.Loading -> WordDetailShimmer()
                    is State.Error -> {
                        val errorState = state as? State.Error
                        errorState?.error?.let { ErrorAlert(it) }
                    }
                    is State.Found -> {
                        val foundState = state as? State.Found
                        foundState?.definitions?.let { WordDetail(it) }
                    }
                }
            }
        }
    }
}
