package com.noahjutz.endict.ui.definitions.definition_content

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.*
import androidx.compose.material.MaterialTheme.colors
import androidx.compose.material.MaterialTheme.typography
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.noahjutz.endict.api.DatabaseDefinition
import com.noahjutz.endict.ui.theme.EndictTheme

@ExperimentalMaterialApi
@ExperimentalFoundationApi
@Composable
fun WordDetail(
    definitions: List<DatabaseDefinition>
) {
    LazyColumn(contentPadding = PaddingValues(bottom = 24.dp)) {
        item {
            Column(
                Modifier
                    .background(colors.primary)
                    .padding(horizontal = 16.dp)
                    .padding(top = 24.dp)
                    .fillMaxWidth()
            ) {
                Text(definitions.first().word, style = typography.h3, color = colors.onPrimary)
                val phonetics = "" // TODO dependent on database
                if (phonetics.isNotBlank()) {
                    Text(
                        phonetics,
                        color = colors.onPrimary.copy(alpha = 0.5f),
                        fontFamily = FontFamily.Monospace,
                    )
                }
                Spacer(Modifier.height(24.dp))
            }
            Spacer(Modifier.height(24.dp))
        }

        itemsIndexed(
            definitions
                .groupBy { it.pos }
                .toList()
        ) { i, (partOfSpeech, definitions) ->
            Column(Modifier.padding(horizontal = 16.dp)) {
                if (i != 0) Spacer(Modifier.height(12.dp))
                if (partOfSpeech.isNotBlank()) {
                    Text(
                        partOfSpeech,
                        color = colors.onSurface.copy(alpha = 0.5f),
                        fontWeight = FontWeight.Light
                    )
                }
                for (definition in definitions) {
                    Row {
                        Text("\u2022", modifier = Modifier.padding(end = 8.dp))
                        Text(definition.text)
                    }
                }
            }
        }

        item {
            Divider(Modifier.padding(top = 32.dp, bottom = 16.dp))
            Text(
                "Sources",
                Modifier.padding(horizontal = 16.dp),
                color = colors.onSurface.copy(alpha = 0.3f),
                fontWeight = FontWeight.Light
            )
        }

        items(definitions.map { it.source }.distinct()) { source ->
            ListItem(
                modifier = Modifier.alpha(0.5f),
                text = {
                    Row {
                        Text("\u2022", Modifier.padding(end = 8.dp))
                        Text(source)
                    }
                }
            )
        }
    }
}

@ExperimentalFoundationApi
@ExperimentalMaterialApi
@Composable
@Preview
fun WordDetailPreview() {
    EndictTheme {
        Surface {
            WordDetail(emptyList())
        }
    }
}
