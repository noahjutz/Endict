package com.noahjutz.endict.ui.translate.result_content

import androidx.compose.animation.animateColor
import androidx.compose.animation.core.*
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme.colors
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.unit.dp

@Composable
fun TranslationShimmer() {
    val infiniteTransition = rememberInfiniteTransition()
    val onSurfaceShimmer by infiniteTransition.animateColor(
        initialValue = colors.onSurface.copy(alpha = 0.1f),
        targetValue = colors.onSurface.copy(alpha = 0.03f),
        animationSpec = infiniteRepeatable(
            animation = tween(1000, easing = LinearEasing),
            repeatMode = RepeatMode.Reverse
        )
    )

    Text(
        text = " ",
        Modifier
            .padding(16.dp)
            .clip(RoundedCornerShape(topEndPercent = 50, bottomEndPercent = 50))
            .background(onSurfaceShimmer)
            .fillMaxWidth(0.6f)
    )
}
