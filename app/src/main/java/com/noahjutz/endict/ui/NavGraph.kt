package com.noahjutz.endict.ui

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.noahjutz.endict.ui.definitions.search.Search
import com.noahjutz.endict.ui.translate.Translate

@ExperimentalAnimationApi
@ExperimentalFoundationApi
@ExperimentalMaterialApi
@Composable
fun NavGraph(navController: NavHostController) {
    NavHost(navController = navController, startDestination = "search") {
        composable("search") { Search() }
        composable("translate") { Translate() }
    }
}
