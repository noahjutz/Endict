package com.noahjutz.endict.ui.theme

import androidx.compose.ui.graphics.Color

val Primary = Color(0xFF795548)
val PrimaryLight = Color(0xFFa98274)
val PrimaryDark = Color(0xFF4b2c20)

val Secondary = Color(0xFFffd740)
val SecondaryLight = Color(0xFFffff74)