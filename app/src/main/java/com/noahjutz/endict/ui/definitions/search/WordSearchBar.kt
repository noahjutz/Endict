package com.noahjutz.endict.ui.definitions.search

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Clear
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.unit.dp

@ExperimentalAnimationApi
@Composable
fun WordSearchBar(
    value: String,
    onValueChange: (String) -> Unit,
    modifier: Modifier = Modifier,
) {
    BasicTextField(
        value = value,
        onValueChange = onValueChange,
        textStyle = MaterialTheme.typography.h4.copy(color = MaterialTheme.colors.onSurface),
        cursorBrush = SolidColor(MaterialTheme.colors.onSurface),
        singleLine = true,
    ) { innerTextField ->
        Surface(
            modifier = modifier,
            shape = RoundedCornerShape(8.dp),
            elevation = 2.dp,
        ) {
            Row(
                Modifier.padding(horizontal = 16.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Box(
                    Modifier
                        .weight(1f)
                        .padding(vertical = 16.dp)
                ) {
                    if (value.isEmpty()) {
                        Text(
                            "Search",
                            style = MaterialTheme.typography.h4,
                            modifier = Modifier.alpha(0.5f)
                        )
                    }
                    innerTextField()
                }
                Spacer(Modifier.width(8.dp))
                AnimatedVisibility(
                    value.isNotEmpty(),
                    enter = fadeIn(),
                    exit = fadeOut(),
                ) {
                    IconButton(onClick = { onValueChange("") }) {
                        Icon(Icons.Default.Clear, "Clear")
                    }
                }
            }
        }
    }
}
