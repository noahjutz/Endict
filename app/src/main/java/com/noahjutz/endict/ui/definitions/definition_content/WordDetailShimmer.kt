package com.noahjutz.endict.ui.definitions.definition_content

import androidx.compose.animation.animateColor
import androidx.compose.animation.core.*
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme.colors
import androidx.compose.material.MaterialTheme.typography
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.unit.dp

@Composable
fun WordDetailShimmer() {
    val infiniteTransition = rememberInfiniteTransition()
    val onSurfaceShimmer by infiniteTransition.animateColor(
        initialValue = colors.onSurface.copy(alpha = 0.1f),
        targetValue = colors.onSurface.copy(alpha = 0.03f),
        animationSpec = infiniteRepeatable(
            animation = tween(1000, easing = LinearEasing),
            repeatMode = RepeatMode.Reverse
        )
    )

    Column {
        Column(
            Modifier
                .background(colors.primary.copy(alpha = 0.1f))
                .padding(horizontal = 16.dp)
                .padding(top = 24.dp)
                .fillMaxWidth()
        ) {
            Text(
                text = " ",
                Modifier
                    .clip(RoundedCornerShape(topStart = 8.dp, topEnd = 8.dp, bottomEnd = 8.dp))
                    .background(onSurfaceShimmer)
                    .fillMaxWidth(0.8f),
                style = typography.h3,
            )
            Text(
                text = " ",
                Modifier
                    .clip(RoundedCornerShape(bottomStart = 8.dp, bottomEnd = 8.dp))
                    .background(onSurfaceShimmer)
                    .fillMaxWidth(0.4f)
            )
            Spacer(Modifier.height(24.dp))
        }
        Spacer(Modifier.height(24.dp))

        repeat(3) { i ->
            Column(Modifier.padding(horizontal = 16.dp)) {
                if (i != 0) Spacer(Modifier.height(12.dp))
                Text(
                    text = " ",
                    Modifier
                        .clip(RoundedCornerShape(topStart = 8.dp, topEnd = 8.dp))
                        .background(onSurfaceShimmer)
                        .fillMaxWidth(0.2f)
                )
                val iInnerMax = remember { (2..3).random() }
                repeat(iInnerMax) { iInner ->
                    Row {
                        Text(
                            "\u2022",
                            modifier = Modifier.padding(end = 8.dp),
                            color = onSurfaceShimmer
                        )
                        val widthFrac = remember { (35..90).random() / 100f }
                        Text(
                            text = " ",
                            Modifier
                                .clip(
                                    RoundedCornerShape(
                                        topStart = 0.dp,
                                        topEnd = if (iInner == 0) 8.dp else 0.dp,
                                        bottomEnd = if (iInner == iInnerMax - 1) 8.dp else 0.dp,
                                        bottomStart = if (iInner == iInnerMax - 1) 8.dp else 0.dp,
                                    )
                                )
                                .background(onSurfaceShimmer)
                                .fillMaxWidth(widthFrac)
                        )
                    }
                }
            }
        }
    }
}
