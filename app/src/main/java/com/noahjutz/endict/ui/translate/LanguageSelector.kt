package com.noahjutz.endict.ui.translate

import androidx.compose.animation.Crossfade
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.MaterialTheme.colors
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ExpandMore
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import com.noahjutz.endict.api.Language
import com.noahjutz.endict.api.allLanguages

@Composable
fun SelectLanguageButton(
    selected: Language,
    onClick: () -> Unit,
) {
    Crossfade(selected) { language ->
        TextButton(
            onClick = onClick,
            colors = ButtonDefaults.textButtonColors(
                contentColor = colors.onSurface,
                disabledContentColor = colors.onSurface
            ),
            shape = RectangleShape,
        ) {
            Row(
                Modifier
                    .fillMaxWidth()
                    .padding(16.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Image(painterResource(language.flag), null)
                Spacer(Modifier.width(14.dp))
                Text(language.name)
                Spacer(Modifier.weight(1f))
                Icon(Icons.Default.ExpandMore, null)
            }
        }
    }
}

@ExperimentalMaterialApi
@Composable
fun SelectLanguageDialog(
    selected: Language,
    onSelected: (Language) -> Unit,
    onDismissed: () -> Unit,
) {
    Dialog(onDismissRequest = onDismissed) {
        Card(Modifier.padding(vertical = 80.dp), elevation = 0.dp) {
            LazyColumn {
                items(allLanguages) { language ->
                    ListItem(
                        text = { Text(language.name) },
                        icon = {
                            Image(
                                painterResource(language.flag),
                                null,
                                modifier = Modifier
                                    .shadow(2.dp)
                                    .clip(RoundedCornerShape(2.dp)),
                            )
                        },
                        modifier = Modifier
                            .clickable {
                                onSelected(language)
                                onDismissed()
                            }
                            .then(
                                if (language != selected) Modifier else Modifier
                                    .background(colors.primary.copy(alpha = 0.12f))
                            )
                    )
                }
            }
        }
    }
}
