package com.noahjutz.endict.ui.definitions.definition_content

import androidx.compose.animation.Crossfade
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme.colors
import androidx.compose.material.MaterialTheme.typography
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ContentCopy
import androidx.compose.material.icons.filled.Subject
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.unit.dp
import com.noahjutz.endict.util.copyToClipboard

@Composable
fun ErrorAlert(error: Exception) {
    Column(Modifier.verticalScroll(rememberScrollState())) {
        Box(
            Modifier
                .background(colors.error)
                .fillMaxWidth()
                .padding(horizontal = 16.dp, vertical = 24.dp),
        ) {
            Text("Not found :(", style = typography.h3, color = colors.onError)
        }
        Spacer(Modifier.height(24.dp))
        Text(
            error.localizedMessage?.toString() ?: "",
            Modifier.padding(horizontal = 16.dp),
            fontFamily = FontFamily.Monospace
        )
        Card(
            Modifier.padding(16.dp),
            elevation = 0.dp,
            border = BorderStroke(1.dp, colors.onSurface.copy(alpha = 0.12f))
        ) {
            var showStackTrace by remember { mutableStateOf(false) }
            Crossfade(targetState = showStackTrace) {
                if (it) {
                    Column {
                        val context = LocalContext.current
                        TextButton(
                            modifier = Modifier
                                .fillMaxWidth()
                                .height(60.dp),
                            onClick = { context.copyToClipboard(error.stackTraceToString()) }
                        ) {
                            Icon(Icons.Default.ContentCopy, null)
                            Spacer(Modifier.width(12.dp))
                            Text("Copy to clipboard")
                        }
                        Text(
                            error.stackTraceToString(),
                            Modifier
                                .padding(8.dp)
                                .horizontalScroll(rememberScrollState()),
                            fontFamily = FontFamily.Monospace,
                            color = colors.onSurface.copy(alpha = 0.5f),
                            softWrap = false,
                        )
                    }
                } else {
                    TextButton(
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(60.dp),
                        onClick = { showStackTrace = true }
                    ) {
                        Icon(Icons.Default.Subject, null)
                        Spacer(Modifier.width(12.dp))
                        Text("Show Stack Trace")
                    }
                }
            }
        }
    }
}
