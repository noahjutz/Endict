package com.noahjutz.endict.ui.translate

import androidx.compose.animation.Crossfade
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material.*
import androidx.compose.material.MaterialTheme.colors
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.SwapVert
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.unit.dp
import com.github.kittinunf.result.Result
import com.noahjutz.endict.api.Language
import com.noahjutz.endict.api.getTranslation
import com.noahjutz.endict.ui.translate.result_content.TranslationDetail
import com.noahjutz.endict.ui.translate.result_content.TranslationError
import com.noahjutz.endict.ui.translate.result_content.TranslationShimmer
import kotlinx.coroutines.delay

private sealed class State {
    object Loading : State()
    object Empty : State()
    data class Error(val error: Exception) : State()
    data class Found(val translation: String) : State()
}

@ExperimentalMaterialApi
@Composable
fun Translate() {
    var state: State by remember { mutableStateOf(State.Empty) }
    Column(Modifier.padding(24.dp), horizontalAlignment = Alignment.CenterHorizontally) {
        val (inLang, setInLang) = rememberSaveable { mutableStateOf(Language.English) }
        val (outLang, setOutLang) = rememberSaveable { mutableStateOf(Language.Spanish) }
        val (input, setInput) = rememberSaveable { mutableStateOf("") }

        var showDialog by remember { mutableStateOf("") }
        when (showDialog) {
            "inLang" -> SelectLanguageDialog(
                selected = inLang,
                onSelected = setInLang,
                onDismissed = { showDialog = "" }
            )
            "outLang" -> SelectLanguageDialog(
                selected = outLang,
                onSelected = setOutLang,
                onDismissed = { showDialog = "" }
            )
        }

        LaunchedEffect(input, inLang, outLang) {
            if (input.isEmpty()) {
                state = State.Empty
                return@LaunchedEffect
            }
            state = State.Loading
            delay(2000)
            val result = getTranslation(
                query = input,
                inLang = inLang,
                outLang = outLang,
            )
            state = when (result) {
                is Result.Success -> State.Found(result.value)
                is Result.Failure -> State.Error(result.error)
            }
        }

        Card(
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f),
            shape = RoundedCornerShape(24.dp)
        ) {
            Column {
                SelectLanguageButton(
                    selected = inLang,
                ) { showDialog = "inLang" }
                Divider()
                TranslateTextField(input, setInput)
            }
        }

        FloatingActionButton(
            modifier = Modifier.padding(16.dp),
            onClick = { setInLang(outLang.also { setOutLang(inLang) }) },
        ) {
            Icon(Icons.Default.SwapVert, "Swap")
        }

        Card(
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f),
            shape = RoundedCornerShape(24.dp),
        ) {
            Column {
                SelectLanguageButton(
                    selected = outLang,
                ) { showDialog = "outLang" }
                Divider()
                Crossfade(state) {
                    when (it) {
                        is State.Loading -> TranslationShimmer()
                        is State.Error -> {
                            val error =
                                (state as? State.Error)?.error ?: Exception("Error not found")
                            TranslationError(error)
                        }
                        is State.Found -> {
                            val translation = (state as? State.Found)?.translation ?: ""
                            TranslationDetail(translation)
                        }
                        is State.Empty -> Unit
                    }
                }
            }
        }
    }
}

@Composable
private fun TranslateTextField(
    query: String,
    setQuery: (String) -> Unit,
) {
    BasicTextField(
        value = query,
        onValueChange = setQuery,
        textStyle = MaterialTheme.typography.h5.copy(color = MaterialTheme.colors.onSurface),
        cursorBrush = SolidColor(MaterialTheme.colors.onSurface),
    ) { innerTextField ->
        Box(
            Modifier
                .fillMaxSize()
                .padding(16.dp)
        ) {
            if (query.isEmpty()) {
                Text(
                    "Translate",
                    style = MaterialTheme.typography.h5,
                    color = MaterialTheme.colors.onSurface.copy(alpha = 0.5f)
                )
            }
            innerTextField()
        }
    }
}
