package com.noahjutz.endict.ui.definitions.search

import androidx.activity.compose.BackHandler
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.expandVertically
import androidx.compose.animation.shrinkVertically
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@ExperimentalMaterialApi
@ExperimentalFoundationApi
@ExperimentalAnimationApi
@Composable
fun Search() {
    Box(Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        Column {
            val (searchQuery, setSearchQuery) = rememberSaveable { mutableStateOf("") }

            BackHandler(enabled = searchQuery.isNotEmpty()) {
                setSearchQuery("")
            }

            WordSearchBar(
                modifier = Modifier.padding(24.dp),
                value = searchQuery,
                onValueChange = setSearchQuery
            )

            AnimatedVisibility(
                searchQuery.isNotEmpty(),
                enter = expandVertically(Alignment.Top),
                exit = shrinkVertically(Alignment.Top)
            ) {
                SearchBottomSheet(searchQuery)
            }
        }
    }
}
