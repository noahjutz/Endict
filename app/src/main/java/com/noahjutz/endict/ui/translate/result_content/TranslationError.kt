package com.noahjutz.endict.ui.translate.result_content

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme.colors
import androidx.compose.material.MaterialTheme.typography
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.unit.dp

@Composable
fun TranslationError(error: Exception) {
    Box(
        Modifier
            .verticalScroll(rememberScrollState())
            .padding(horizontal = 16.dp)
    ) {
        Column(Modifier.padding(vertical = 16.dp)) {
            Text(
                "Something went wrong...",
                style = typography.h5,
                color = colors.error,
            )
            Spacer(Modifier.height(8.dp))
            Text(
                error.localizedMessage ?: "",
                fontFamily = FontFamily.Monospace,
            )
        }
    }
}
