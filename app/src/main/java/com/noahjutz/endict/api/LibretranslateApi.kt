package com.noahjutz.endict.api

import android.util.Log
import androidx.annotation.DrawableRes
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.Headers
import com.github.kittinunf.fuel.coroutines.awaitObjectResult
import com.github.kittinunf.fuel.serialization.kotlinxDeserializerOf
import com.github.kittinunf.result.Result
import com.github.kittinunf.result.map
import com.noahjutz.endict.R
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import kotlinx.serialization.Serializable

enum class Language(
    val code: String,
    @DrawableRes val flag: Int
) {
    English("en", R.drawable.flag_gb),
    Arabic("ar", R.drawable.flag_sa),
    Chinese("zh", R.drawable.flag_cn),
    French("fr", R.drawable.flag_fr),
    German("de", R.drawable.flag_de),
    Hindi("hi", R.drawable.flag_in),
    Indonesian("id", R.drawable.flag_id),
    Irish("ga", R.drawable.flag_ie),
    Italian("id", R.drawable.flag_it),
    Japanese("ja", R.drawable.flag_jp),
    Korean("ko", R.drawable.flag_kr),
    Polish("pl", R.drawable.flag_pl),
    Portuguese("pt", R.drawable.flag_pt),
    Russian("ru", R.drawable.flag_ru),
    Spanish("es", R.drawable.flag_es),
    Turkish("tr", R.drawable.flag_tr),
    Vietnamese("va", R.drawable.flag_vn)
}

val allLanguages = Language.values().toList().sortedBy { it.name }

@Serializable
data class LtTranslation(val translatedText: String)

private fun postBody(
    q: String,
    source: String,
    target: String,
) = "{\"q\":\"${
q
    .replace("\\", "\\\\")
    .replace("\"", "\\\"")
    .replace("\n", "\\n")
}\",\"source\":\"$source\",\"target\":\"$target\"}".also { Log.d("LibreTranslateApi", it) }

suspend fun getTranslation(
    query: String,
    inLang: Language,
    outLang: Language
): Result<String, Exception> = withContext(IO) {
    return@withContext Fuel.post("https://n.sly.io/translate")
        .body(postBody(query, inLang.code, outLang.code))
        .header(Headers.CONTENT_TYPE, "application/json")
        .awaitObjectResult<LtTranslation>(kotlinxDeserializerOf())
        .map { it.translatedText }
}
