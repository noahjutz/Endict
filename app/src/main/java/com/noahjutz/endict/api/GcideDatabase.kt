package com.noahjutz.endict.api

import android.content.Context
import androidx.room.*

@Entity(tableName = "Definitions")
data class DatabaseDefinition(
    @PrimaryKey val id: Int,
    val word: String,
    val text: String,
    val source: String,
    val pos: String,
)

@Dao
interface DefinitionDao {
    @Query("SELECT * FROM Definitions")
    fun getDefinitions(): List<DatabaseDefinition>

    @Query("SELECT * FROM Definitions WHERE UPPER(word) == UPPER(:word)")
    fun getDefinitions(word: String): List<DatabaseDefinition>
}

@Database(
    entities = [DatabaseDefinition::class],
    version = 3
)
abstract class GcideDatabase : RoomDatabase() {
    abstract fun definitionDao(): DefinitionDao

    companion object {
        @Volatile
        private var INSTANCE: GcideDatabase? = null

        fun getDatabase(context: Context): GcideDatabase = INSTANCE ?: synchronized(this) {
            val instance = Room
                .databaseBuilder(
                    context.applicationContext,
                    GcideDatabase::class.java,
                    "gcide.db"
                )
                .createFromAsset("gcide.db")
                .fallbackToDestructiveMigration()
                .build()
            INSTANCE = instance
            instance
        }
    }
}
